################
# DNS RECORDS
################

resource "cloudflare_zone" "zone_dylanmtaylor_com" {
  paused = false
  plan   = "free"
  type   = "full"
  zone   = "dylanmtaylor.com"
}

## A Records

resource "cloudflare_record" "apps" {
  name    = "apps"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = var.dylanmtaylor_server_ip
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "blog" {
  name    = "blog"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = var.dylanmtaylor_server_ip
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "dev" {
  name    = "dev"
  proxied = false
  ttl     = 1
  type    = "A"
  value   = "127.0.0.1"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "dylanmtaylor_com" {
  name    = "dylanmtaylor.com"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = var.dylanmtaylor_server_ip
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "files" {
  name    = "files"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = var.dylanmtaylor_server_ip
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "fwc" {
  name    = "fwc"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = var.dylanmtaylor_server_ip
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "git" {
  name    = "git"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = var.dylanmtaylor_server_ip
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "www" {
  name    = "www"
  proxied = true
  ttl     = 1
  type    = "A"
  value   = var.dylanmtaylor_server_ip
  zone_id = var.cloudflare_zone_id
}

## AAAA Records

resource "cloudflare_record" "apps_ipv6" {
  name    = "apps"
  proxied = true
  ttl     = 1
  type    = "AAAA"
  value   = var.dylanmtaylor_server_ipv6
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "blog_ipv6" {
  name    = "blog"
  proxied = true
  ttl     = 1
  type    = "AAAA"
  value   = var.dylanmtaylor_server_ipv6
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "dev_ipv6" {
  name    = "dev"
  proxied = false
  ttl     = 1
  type    = "AAAA"
  value   = "::1"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "dylanmtaylor_com_ipv6" {
  name    = "dylanmtaylor.com"
  proxied = true
  ttl     = 1
  type    = "AAAA"
  value   = var.dylanmtaylor_server_ipv6
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "files_ipv6" {
  name    = "files"
  proxied = true
  ttl     = 1
  type    = "AAAA"
  value   = var.dylanmtaylor_server_ipv6
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "fwc_ipv6" {
  name    = "fwc"
  proxied = true
  ttl     = 1
  type    = "AAAA"
  value   = var.dylanmtaylor_server_ipv6
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "git_ipv6" {
  name    = "git"
  proxied = true
  ttl     = 1
  type    = "AAAA"
  value   = var.dylanmtaylor_server_ipv6
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "www_ipv6" {
  name    = "www"
  proxied = true
  ttl     = 1
  type    = "AAAA"
  value   = var.dylanmtaylor_server_ipv6
  zone_id = var.cloudflare_zone_id
}

## CNAME Records

resource "cloudflare_record" "autodiscover" {
  name    = "autodiscover"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "autodiscover.outlook.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "domainconnect" {
  name    = "_domainconnect"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "_domainconnect.gd.domaincontrol.com"
  zone_id = var.cloudflare_zone_id
}

## TXT Records

resource "cloudflare_record" "email_dmarc_record" {
  name    = "_dmarc"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=DMARC1; p=none; rua=mailto:dylan@dylanmtaylor.com"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "google_site_verification" {
  name    = "dylanmtaylor.com"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "google-site-verification=JvKu7bm06rCAIp_Ty2xk0fuBC9JVqhXHSMuEHAqQjvQ"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "dylanmtaylor_com_spf" {
  name    = "dylanmtaylor.com"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=spf1 include:outlook.com include:spf.protection.outlook.com ~all"
  zone_id = var.cloudflare_zone_id
}

resource "cloudflare_record" "dylanmtaylor_com_outlook" {
  name    = "_outlook"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "145002896"
  zone_id = var.cloudflare_zone_id
}

## MX Records
resource "cloudflare_record" "dylanmtaylor_com_mx" {
  name     = "dylanmtaylor.com"
  priority = 0
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "145002896.pamx1.hotmail.com"
  zone_id  = var.cloudflare_zone_id
}

##############
# PAGE RULES #
##############

resource "cloudflare_page_rule" "dylanmtaylor_root_page_rule" {
  priority = 2
  status   = "active"
  target   = "http://dylanmtaylor.com/"
  zone_id  = var.cloudflare_zone_id
  actions {
      always_use_https = true
  }
}

resource "cloudflare_page_rule" "dylanmtaylor_wildcard_page_rule" {
  priority = 1
  status   = "active"
  target   = "http://*.dylanmtaylor.com/"
  zone_id  = var.cloudflare_zone_id
  actions {
      always_use_https = true
  }
}

